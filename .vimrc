" vim: set foldmethod=marker foldlevel=0 nomodeline:

" ========================================
" Generic settings : {{{
" ========================================
set nocompatible
syntax on
set vb
filetype indent plugin on
filetype on
set complete -=i
set ignorecase
set smartcase
set wildmenu
set path +=** " tab-completion for all files
set hidden " ctrlspace requires this
set autoread
set cursorline " current line is highlighted
set gdefault
set splitright
set number "current line number
set relativenumber " other lines are relative to the current one
set hlsearch " search results are highlighted
set lazyredraw " the screen isn't redrawn every time something happens
augroup remember_folds " keep folds when exiting a file
    autocmd!
    autocmd BufWinLeave * mkview
    autocmd BufWinEnter * silent! loadview
augroup END
set noswapfile " no backups
set nowb
set nobackup
set enc=utf-8
set fileencoding=utf-8
set ttimeout
set timeoutlen=1000
set ttimeoutlen=10
set updatetime=100
set ruler
set backspace=eol,start,indent
set whichwrap+=<,>,h,l
set t_Co=256
set background=dark
set expandtab smarttab shiftwidth=4 tabstop=4
set ai
set si
set wrap
" }}}
" ========================================
" Plugins : {{{
" ========================================
" github.com/junegunn/vim-plug
call plug#begin('~/.vim/plugged')

    " Generali
    Plug 'vim-airline/vim-airline'

    "" Temi
    Plug 'tomasiser/vim-code-dark'
    Plug 'vim-airline/vim-airline-themes'

    "" Navigazione
    Plug 'ervandew/supertab'
    Plug 'majutsushi/tagbar'
    Plug 'christoomey/vim-tmux-navigator'
    Plug 'easymotion/vim-easymotion'
    Plug 'tpope/vim-surround'
    Plug 'tpope/vim-commentary'
    Plug 'tpope/vim-vinegar'
    Plug 'scrooloose/nerdtree', {'on' : 'NERDTreeToggle'}

    " Programmazione
    Plug 'artur-shaik/vim-javacomplete2', { 'for' : 'java' }
    Plug 'scrooloose/syntastic', { 'for': ['java', 'c', 'cpp', 'py', 'php', 'html'] }
    Plug 'sheerun/vim-polyglot', { 'for': ['java', 'c', 'cpp', 'py', 'php', 'html'] }
    Plug 'LeonB/HTML-AutoCloseTag', { 'for': ['html', 'php'] }
    Plug 'lervag/vimtex', { 'for': ['tex', 'latex'] }
    Plug 'xuhdev/vim-latex-live-preview', { 'for': ['tex', 'latex'] }
    if has('nvim')
        Plug 'Shougo/deoplete.nvim'
    else
        Plug 'Shougo/deoplete.nvim'
        Plug 'roxma/nvim-yarp'
        Plug 'roxma/vim-hug-neovim-rpc'
    endif

    " Altro
    Plug 'nathanaelkane/vim-indent-guides'
    Plug 'jiangmiao/auto-pairs'
    Plug 'vim-ctrlspace/vim-ctrlspace'
    Plug 'ctrlpvim/ctrlp.vim'
    Plug 'junegunn/vim-peekaboo'
    Plug 'AndrewRadev/splitjoin.vim'
    Plug 'mbbill/undotree', { 'on': 'UndotreeToggle' }
    Plug 'junegunn/vim-easy-align'

    """ git integration
    Plug 'tpope/vim-fugitive'
    Plug 'airblade/vim-gitgutter'
    """ snippets
    Plug 'Valloric/YouCompleteMe'
    Plug 'SirVer/ultisnips'
    Plug 'garbas/vim-snipmate'
    Plug 'MarcWeber/vim-addon-mw-utils'
    Plug 'tomtom/tlib_vim'
    Plug 'honza/vim-snippets'
call plug#end ()
" }}}
" ========================================
" Key bindings : {{{
" ========================================

nnoremap <silent> <leader>, :noh<cr>

" j and k can navigate multiline text
nnoremap j gj
nnoremap k gk

imap     jk <ESC>

nmap <leader>w :w!<cr>

nnoremap <silent> U :UndotreeToggle<CR><Paste>

command W w !sudo tee % > /dev/null

" Tmux navigator
nnoremap <silent> <c-h> :TmuxNavigateLeft<cr>
nnoremap <silent> <c-j> :TmuxNavigateDown<cr>
nnoremap <silent> <c-k> :TmuxNavigateUp<cr>
nnoremap <silent> <c-l> :TmuxNavigateRight<cr>
nnoremap <silent> <c-/> :TmuxNavigatePrevious<cr>
" Save on panel switch
let g:tmux_navigator_save_on_switch = 2

map  <silent> <C-p> :CtrlSpace<CR>
imap <silent> <C-p> <Esc>:CtrlSpace<CR>

" Spellcheck
"" Italian
map <silent> <F5> :setlocal spell! spelllang=it<CR>
"" American english (color, not colour)
map <silent> <F6> :setlocal spell! spelllang=en_us<CR>

map <F8> :TagbarToggle<CR>

" LaTeX autopreview
autocmd BufNewFile,BufRead *.tex,*.latex map <F3> :LLPStartPreview<CR>:set updatetime=1000<CR>
autocmd BufNewFile,BufRead *.tex,*.latex inoremap <Space><Tab>           <ESC>/<++><Enter>"_c4l
autocmd BufNewFile,BufRead *.tex,*.latex vnoremap <Space><Tab>           <ESC>/<++><Enter>"_c4l
" C/C++ compilation
autocmd FileType,BufRead cpp    map <F10> :!g++ % && ./a.out && rm ./a.out<CR>
autocmd FileType,BufRead c      map <F10> :!gcc % && ./a.out && rm ./a.out<CR>
autocmd FileType,BufRead c,cpp  map <F9>  :!ctags -R .<CR><CR>
" Java autocomplete
autocmd FileType,BufRead java   setlocal omnifunc=javacomplete#Complete
" PHP check
autocmd FileType,BufRead php    map <F9>  :!php -l %<CR>

" :terminal
tnoremap <ESC><ESC> <C-\><C-N>
noremap <C-t> <ESC>:vsplit<CR>:terminal<CR>i
tnoremap <silent> <C-t> <C-\><C-N>:q<CR>
tnoremap <silent> <C-d> <C-\><C-N>:q<CR>

" Easier buffer manipulation
map             bn <Esc>:e<Space>
map  <silent>   bt <Esc>:bn<CR>
map  <silent>   bT <Esc>:bp<CR>
map  <silent>   bx <Esc>:bd!<CR>
vmap <silent>   bt :bn<CR>
vmap <silent>   bT :bp<CR>
vmap <silent>   bc :bd<CR>

xmap ga <Plug>(EasyAlign)
nmap ga <Plug>(EasyAlign)
" }}}
" ========================================
" Variabili : {{{
" ========================================
let g:deoplete#enable_at_startup = 1
let g:airline_powerline_fonts = 1
let g:airline#extensions#tabline#enabled = 1
"" compatibilità tra YCM e UltiSnips
let g:ycm_key_list_select_completion = ['<C-j>', '<Down>']
let g:ycm_key_list_previous_completion = ['<C-k>', '<Up>']
let g:SuperTabDefaultCompletionType = '<C-j>'
"" rebind UltiSnipsExpandTrigger
let g:UltiSnipsExpandTrigger = "<tab>"
let g:UltiSnipsJumpForwardTrigger = "<tab>"
let g:UltiSnipsJumpBackwardTrigger = "<s-tab>"
"" rebind ctrlp a ctrl+i
let g:ctrlp_map = '<c-i>'
let g:ctrlp_cmd = 'CtrlP'
"" Undotree
let g:undotree_WindowLayout = 2
let mapleader = "\\"
let g:mapleader = "\\"
set wildignore=*.o,*~,*.pyc,*.class,*.out,*/.git/*,*/.hg/*,*/.svn/*,*/.DS_Store,*.jpg,*.png
colorscheme codedark
" }}}
