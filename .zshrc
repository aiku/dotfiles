ZSH_THEME="agnoster"
# export PATH=$HOME/bin:/usr/local/bin:$PATH

export ZSH=~/.oh-my-zsh
export SHELL=/bin/zsh

# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes
# See https://github.com/powerline/fonts

HYPHEN_INSENSITIVE="true"

ENABLE_CORRECTION="true"

plugins=(
  git
)

source $ZSH/oh-my-zsh.sh
export LANG=en_US.UTF-8
export TERM='screen-256color'
export EDITOR='vim'

export SSH_KEY_PATH="~/.ssh/rsa_id"

# alias
source ~/dotfiles/alias.txt

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
