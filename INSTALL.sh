#!/bin/bash
echo "If you want to backup your file CTRL-C this script, do what you have to and then run it again. I won't backup anything."
echo "Press enter if you want to continue. No further confirmation will be asked."
read -n 1
distro=`cat /etc/*-release | grep "^ID=" | grep -E -o "[a-z]\w+"`

basic_pkgs=(
zsh
vim
w3m
)

if [ "$distro" = "debian" ] || [ "$distro" = "raspbian" ]; then
    echo "Looks like you're running either Debian or Raspbian. I will use apt to update the current packages and then install some."
    sudo apt update
    sudo apt upgrade
    sudo apt install $basic_pkgs
    sudo apt install $( cat ~/dotfiles/apt_pi.list )

elif [ "$distro" = "arch" ]; then
    echo "Looks like you fell for the meme and are now running Arch. I will use pacman to update all the packages and then install some."
    sudo pacman -Syyu
    echo "I need pacaur to proceed. I will install that first."
    yaourt -S pacaur
    echo "Now I will proceed to install all the packages I need."
    sudo pacaur -S --noedit -q - < ~/dotfiles/pacman_arch.txt
elif [ "$distro" == "gentoo" ]; then
    echo "Looks like you fell for the biggest meme on this whole planet. I'm going to use emerge to do all my stuff, go watch a movie and maybe I'll be finished by then."
    emerge-webrsync
    emerge -uDU --keep-going --with-bdeps=y @world
    emerge -a dev-vcs/git
else
    echo "Looks like you're using some other distro. I can't help you."
    exit
fi

echo "Great, now I am going to remove the current configuration files and override them with symlinks to the ones in this repo."
rm -r ~/.config/i3          | mkdir ~/.config/i3
rm -r ~/.config/nvim        | mkdir ~/.config/nvim
rm -r ~/.config/qutebrowser | mkdir ~/.config/qutebrowser
rm -r ~/.config/ranger      | mkdir ~/.config/ranger
rm -r ~/.ncmpcpp
rm    ~/.bashrc
rm    ~/.zshrc
rm    ~/.vimrc
rm    ~/.spacemacs
rm    ~/.emacs.d/init.el

ln -s ~/dotfiles/i3/*                 ~/.config/i3/
ln -s ~/dotfiles/nvim/*               ~/.config/nvim/
ln -s ~/dotfiles/qutebrowser/*        ~/.config/qutebrowser
ln -s ~/dotfiles/ranger/*             ~/.config/ranger
ln -s ~/dotfiles/.bashrc              ~/.bashrc
ln -s ~/dotfiles/.zshrc               ~/.zshrc
ln -s ~/dotfiles/.vimrc               ~/.vimrc
ln -s ~/dotfiles/emacs/.spacemacs     ~/.spacemacs
ln -s ~/dotfiles/emacs/init.el        ~/.emacs.d/init.el
ln -s ~/dotfiles/.ncmpcpp             ~/.ncmpcpp
ln -s ~/dotfiles/init.vim 		      ~/.vim/init.vim

echo "I will also install oh-my-zsh, some plugins, and set zsh as your default shell."
cd ~
# Yes, I know this isn't safe. But I really want it to be simple as possible, with little to no input user.
sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ~/.oh-my-zsh/plugins/zsh-syntax-highlighting
chsh -s /bin/zsh

echo "I will now proceed to install vim-plug for vim. Next time you run vim or neovim, remember to use :PlugInstall  to install all the plugins."
curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

echo "Looks like we're done here."
